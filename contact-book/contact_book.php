<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Contact book</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
  </head>
  <body>
    <?php
      require_once "config.php";
      $db = new mysqli($server,$user,$pass,$db);
    ?>
    <div class="container">
    <h1>Contact book</h1>
    <div class="info">
      <?php
        list($contacts) = $db->query("SELECT COUNT(*) FROM contacts")->fetch_row();
        list($friends) = $db->query("SELECT COUNT(*) FROM contacts WHERE is_friend = 1")->fetch_row();
      ?>
      <p>Number of contacts: <?php echo $contacts; ?> </p>
      <p>Number of friends: <?php echo $friends; ?></p>
      <p><form action="contact_book.php" method="get"><label for="filter">Search by last name</label><input type="text" name="filter"></form>
      <p><button data-toggle="modal" data-target="#create_contact" class="create">Create a contact</button></p>
      <div id="create_contact" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Create a contact</h4>
            </div>
            <div class="modal-body">
              <form method="post" action="dboperate.php" id="contact">
                <input type="hidden" name="action">
                <input type="hidden" name="id">
                <div class="form-group">
                  <input type="text" placeholder="First name" name="first_name" />
                </div>
                <div class="form-group">
                  <input type="text" placeholder="Last name" name="last_name" />
                </div>
                <div class="form-group">
                  <input type="text" placeholder="Phone number" name="phone" />
                </div>
                <div class="form-group">
                  <input type="text" placeholder="E-mail" name="email" />
                </div>
                <div class="form-group">
                  <input type="text" placeholder="Address" name="address" />
                </div>
                <div class="form-group">
                  <input type="text" placeholder="City" name="city" />
                </div>
                <div class="form-group">
                  <input type="text" placeholder="Zip" name="zip" />
                </div>
                <div class="form-group">
                  <label for="friend">Is friend?</label>
                  <input type="checkbox" name="friend" value="1" />
                </div>
                <button type="submit" class="btn btn-default">Create</button>
              </form>
          </div>
        </div>
      </div>
    </div>


    <table class="table">
      <thead>
        <tr>
          <th><a href="?sort=name">First name</a></th>
          <th><a href="?sort=last_name">Last name</a></th>
          <th><a href="?sort=phone_number">Phone number</a></th>
          <th><a href="?sort=email">Email</a></th>
          <th><a href="?sort=address">Address</a></th>
          <th><a href="?sort=city">City</a></th>
          <th><a href="?sort=zip">Zip</a></th>
          <th><a href="?sort=is_friend">Is Friend</a></th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $adds="";
          if(isset($_GET['filter'])) $adds .= " WHERE last_name LIKE '%".$_GET['filter']."%'";
          if(isset($_GET['sort'])) $adds .= " ORDER BY ".$_GET['sort'];
          if(isset($_GET['page'])){
            $start = ($_GET['page']-1)*10;
            $stop = $start+9;
            $adds .= " LIMIT $start,$stop";
          }
          $results = $db->query("SELECT * FROM contacts$adds");
          while(list($id,$first_name,$last_name,$phone,$email,$address,$city,$zip,$is_friend) = $results->fetch_row()){
            echo "<tr>
                    <td class='first_name'>$first_name</td>
                    <td class='last_name'>$last_name</td>
                    <td class='phone'>$phone</td>
                    <td class='email'>$email</td>
                    <td class='address'>$address</td>
                    <td class='city'>$city</td>
                    <td class='zip'>$zip</td>
                    <td class='friend'>";

            if($is_friend==1) echo "yes";
            else echo "no";
            echo "</td>  <td>
                      <button id='$id' class='edit' data-toggle='modal'  data-target='#create_contact'>Edit</button>
                      <form method='POST' action='dboperate.php' id='delete'>
                        <input type='hidden' name='id' value='$id' />
                        <input type='hidden' name='action' value='delete' />
                        <button>Delete</button>
                      </form>
                    </td>
                  </tr>";
          }
        ?>
      </tbody>
    </table>
    <p>Pages:
    <?php
      $i=1;
      while($contacts>=0){
        echo "<a href='?page=$i'>1</a> ";
        $contacts-=10;
      }
    ?>
  </p>
  </div>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $("button.create").click(function(){
        $("#create_contact input[name='action']").val("create");
        $("#contact button").html("Create");
      });
      $("#contact").validate({
        submitHandler: function(form){
          form.submit();
        },
        rules:{
          first_name: "required",
          last_name: "required",
          phone: {
            required: true,
            minlength: 9,
            maxlength: 9,
            digits: true
          },
          email: {
            required: true,
            email: true
          },
          address: "required",
          city: "required",
          zip: { required: true, digits: true}

        }
      });

      $("button.edit").click(function(){
        document.getElementById("contact").reset();
        $("#contact input[name='action']").val("edit");
        $("#contact input[name='id']").val($(this).attr("id"));
        $("#contact input[name='first_name']").val($(this).parent().siblings(".first_name").html());
        $("#contact input[name='last_name']").val($(this).parent().siblings(".last_name").html());
        $("#contact input[name='phone']").val($(this).parent().siblings(".phone").html());
        $("#contact input[name='email']").val($(this).parent().siblings(".email").html());
        $("#contact input[name='address']").val($(this).parent().siblings(".address").html());
        $("#contact input[name='city']").val($(this).parent().siblings(".city").html());
        $("#contact input[name='zip']").val($(this).parent().siblings(".zip").html());
        if($(this).parent().siblings(".friend").html()=="yes") $("#contact input[name='friend']").prop('checked',true);
        $("#contact button").html("Edit");
      });
      $("#delete").submit(function(){
        if(confirm("Are you sure that you want to delete this contact?")){
          return true;
        }else{
          return false;
        }
      })
    </script>
  </body>
</html>
